#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd
from datetime import datetime
#import setup_times_calc
import workers_availability_repl

pd.set_option("display.max_rows", None, "display.max_columns", None)


def simulation_past(machines_df, orders_df, workers, dsl):
    print(datetime.now(), " - Start of Past Simulation.")
    daily_setup_times = [0] * 800
    workers_list = [[math.inf] for i in range(workers)]
    solutions_past = {}
    sequence = {}
    loom_load = {}
    loom_last = []
    for m in machines_df.index:
        dummy_df = orders_df.loc[orders_df['loom_id'] == m]
        dummy_df = dummy_df.sort_values(by='start_date')
        sequence[m] = list(dummy_df.index)
        loom_load[m] = 0
        if len(dummy_df.index) >0:
            loom_last.append(dummy_df.index[0])
    for i in orders_df.index:
        if orders_df.loc[i, 'incom'] <= 6:
            orders_df.loc[i, 'loomeff'] = 0.6
        elif orders_df.loc[i, 'incom'] <= 14:
            orders_df.loc[i, 'loomeff'] = 0.5
        else:
            orders_df.loc[i, 'loomeff'] = 0.4
        orders_df.loc[i, 'process_time'] = (orders_df.loc[i, 'length_mt'] * orders_df.loc[i, 'strokes_per_mt'] *
                                            orders_df.loc[i, 'ybf']) / (
                                                   machines_df.loc[
                                                       np.int64(orders_df.loc[i, 'loom_id']), 'loomSpeed'] *
                                                   orders_df.loc[i, 'loomeff'])

        if sequence[orders_df.loc[i, 'loom_id']].index(i) == 0:
            orders_df.loc[i, 'setup_time'] = 0
        else:
            last_order = sequence[orders_df.loc[i, 'loom_id']][sequence[orders_df.loc[i, 'loom_id']].index(i) - 1]
            orders_df.loc[i, 'setup_time'] = \
                knotting(orders_df.loc[i, 'ca'], orders_df.loc[i, 'cc'], orders_df.loc[i, 'yarns'],
                         orders_df.loc[i, 'comb'], orders_df.loc[i, 'comb_height'], orders_df.loc[last_order, 'ca'],
                         orders_df.loc[last_order, 'cc'], orders_df.loc[last_order, 'yarns'],
                         orders_df.loc[last_order, 'comb'], orders_df.loc[last_order, 'comb_height'])
    # print(orders_df.head(10))
    # print (workers)

    current_time = 0

    jobs_list = list(orders_df.index)
    # print(len(loom_last))
    # print(len(machines_df.index))
    # print(orders_df.head())
    #print(orders_df.head(10))
    # dupl = pd.concat(g for _, g in orders_df.groupby("chainType") if len(g) > 1)

    while len(jobs_list) > 0:
        for m in machines_df.index:
            if len(sequence[m]) > 0:
                last_order = sequence[m][0]
                if orders_df.loc[last_order, 'start_date'] <= current_time:
                    # ----------------------------------------------
                    earliest_worker_start = np.full((workers, 1), np.inf)
                    setup_time = orders_df.loc[last_order, 'setup_time']
                    process_time = orders_df.loc[last_order, 'process_time']
                    if setup_time > 0:
                        for worker_resource in range(workers):
                            earliest_worker_start[
                                worker_resource] = workers_availability_repl.workers_availability_repl(
                                daily_setup_times[:],
                                workers_list[worker_resource][:],
                                loom_load[m],
                                setup_time, dsl)
                        chosen_worker = np.argmin(earliest_worker_start)
                        starting_loom = int(earliest_worker_start[chosen_worker])
                        workers_list[int(chosen_worker)].extend(
                            [int(earliest_worker_start[chosen_worker]),
                             int(earliest_worker_start[chosen_worker] + setup_time)])
                        workers_list[int(chosen_worker)].sort()
                        daily_setup_times[math.floor((earliest_worker_start[chosen_worker]) / 1440)] += setup_time
                    # ----------------------------------------------
                    else:
                        starting_loom = loom_load[m]
                    loom_load[m] = starting_loom + setup_time + process_time
                    orders_df.loc[last_order, 'end_time'] = loom_load[m]
                    jobs_list.remove(last_order)
                    sequence[m].remove(last_order)
                else:
                    loom_load[m] = max(current_time, loom_load[m])
            # print (len(sequence[m]))]
            loom_load[m] = max(current_time, loom_load[m])
        current_time += 1440
    solutions_past['total_orders'] = len(orders_df.chainType.unique())
    solutions_past['energy_consumption(%)'] = 0
    solutions_past['tardy_jobs(%)'] = 0
    solutions_past['tardiness(%)'] = 0
    # makespan, tardiness, energy_cons, tardy-jobs
    done_jobs = []
    for i in orders_df.index:
        chainType = orders_df.loc[i, 'chainType']
        if chainType not in done_jobs:
            dummy_df = orders_df.loc[orders_df['chainType'] == chainType]
            orders_df.loc[i, 'total_end'] = dummy_df.loc[i, 'end_time'].max()
            # print (dummy_df.head())
            # dummy_df = dummy_df.sort_values(by='start_date')
            # orders_df.loc[i, 'tardiness'] = max(0, orders_df.loc[i, 'total_end'] - orders_df.loc[i, 'delivery_date'])
            if orders_df.loc[i, 'total_end'] > orders_df.loc[i, 'delivery_date']:
                solutions_past['tardiness(%)'] += orders_df.loc[i, 'total_end'] - orders_df.loc[i, 'delivery_date']
                solutions_past['tardy_jobs(%)'] += 1
            done_jobs.append(chainType)
        solutions_past['energy_consumption(%)'] += 7.5 * orders_df.loc[i, 'process_time'] / 60
    solutions_past['energy_consumption(%)'] = round(solutions_past['energy_consumption(%)'], 3)
    solutions_past['tardiness(%)'] = round(solutions_past['tardiness(%)'], 2)
    solutions_past['makespan(%)'] = orders_df.total_end.max()
    print(datetime.now(), " - End of Past Simulation.")
    return solutions_past, orders_df


def knotting(ca_new, cc_new, yarns_new, comb_new, combHeight_new, ca_old, cc_old, yarns_old, comb_old, combHeight_old):
    if (ca_new == ca_old) and (
            cc_new == cc_old):
        setup = 0
    elif (ca_new == ca_old) or (
            yarns_new == yarns_old and (
            comb_new == comb_old)):
        # KNOTTING SETUP TYPE
        if yarns_new <= 3000:
            # EASY KNOTTING
            setup = 4 * 60
        elif yarns_new <= 5000:
            # MEDIUM KNOTTING
            setup = 4 * 60
        else:
            # HARD KNOTTING
            setup = 6 * 60
    else:
        # FULL SETUP
        if abs(combHeight_new - combHeight_old) <= 10:
            # EASY CHANGE
            setup = 4 * 60
        elif abs(combHeight_new - combHeight_old) <= 15:
            # MEDIUM CHANGE
            setup = 6 * 60
        else:
            # HARD CHANGE
            setup = 8 * 60

    return setup
