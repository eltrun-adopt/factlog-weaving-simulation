#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import pandas as pd
from datetime import datetime
import warnings

warnings.filterwarnings("ignore")

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)


def repl_calculator(refined_data):
    print(datetime.now(), " - Start of Objectives Calculator.")

    # Start Date

    startDate = refined_data["startDate"]
    startDate = datetime.strptime(startDate, '%m/%d/%Y %H:%M:%S')
    endDate = refined_data["endDate"]
    endDate = datetime.strptime(endDate, '%m/%d/%Y %H:%M:%S')

    dayStart = refined_data["startDate"]
    dayStart = dayStart.split(' ')[0] + " 00:00:00"
    dayStart = datetime.strptime(dayStart, '%m/%d/%Y %H:%M:%S')

    hist_data = pd.DataFrame.from_dict(refined_data["historical_data"]).T

    hist_data['chainType'] = hist_data['chain_id']
    hist_data['chainType'] = hist_data['chainType'].apply(str)
    hist_data['chainType'] = hist_data['chainType'] + hist_data['type']

    hist_data = hist_data.loc[hist_data['working_hours'] != 0]
    final_df = pd.DataFrame(index=hist_data.chainType.unique(),
                            columns=['start_timestamp', 'end_timestamp', 'delivery_date'])
    for i in final_df.index:
        dummy_df = hist_data.loc[hist_data['chainType'] == i]
        # final_df.loc[i, 'chainID'] = dummy_df['chain_id'].min()
        dummy_df['start_timestamp'] = pd.to_datetime(dummy_df['start_timestamp'], format='%d/%m/%Y %H:%M')
        dummy_df['end_timestamp'] = pd.to_datetime(dummy_df['end_timestamp'], format='%d/%m/%Y %H:%M')
        dummy_df['delivery_date'] = pd.to_datetime(dummy_df['delivery_date'], format='%d/%m/%Y') + pd.Timedelta(days=1)
        final_df.loc[i, 'start_timestamp'] = dummy_df['start_timestamp'].min()
        final_df.loc[i, 'end_timestamp'] = dummy_df['end_timestamp'].max()
        final_df.loc[i, 'delivery_date'] = dummy_df['delivery_date'].max()
        # final_df.loc[i, 'energy_cons'] = dummy_df['consumption_kwh'].sum()
        final_df.loc[i, 'tardiness'] = max(0, (
                final_df.loc[i, 'end_timestamp'] - final_df.loc[i, 'delivery_date']).total_seconds()) / 60
        final_df.loc[i, 'tardy'] = 0 if final_df.loc[i, 'tardiness'] <= 0 else 1
    # print(final_df.head())

    solutions = {}
    # solutions['replication'] = {}
    solutions = {"total_orders": int(len(final_df.index)), 'tardiness(mins)': round(final_df.tardiness.sum(), 2),
                 'tardy_jobs': int(final_df.tardy.sum()),
                 'energy_consumption(kWh)': round(hist_data.consumption_kwh.sum(), 3), 'makespan': refined_data["endDate"]}
    # 'makespan': str(final_df.end_timestamp.max())}
    '''
    for k in machines:
        if k["maintenanceStart"] is not None and k["maintenanceEnd"] is not None:
            # k["maintenanceStart"] = k["maintenanceStart"].replace(",", "")
            k["maintenanceStart"] = datetime.strptime(k["maintenanceStart"], '%m/%d/%Y %H:%M:%S')
            c = k["maintenanceStart"] - dayStart
            k["maintenanceStart"] = c.total_seconds() / 60
            # k["maintenanceEnd"] = k["maintenanceEnd"].replace(",", "")
            k["maintenanceEnd"] = datetime.strptime(k["maintenanceEnd"], '%m/%d/%Y %H:%M:%S')
            c = k["maintenanceEnd"] - dayStart
            k["maintenanceEnd"] = c.total_seconds() / 60
            k["maintenanceDuration"] = k["maintenanceEnd"] - k["maintenanceStart"]
        else:
            k["maintenanceDuration"] = 0
        k["currentLoad"] = extraLoad + k["maintenanceDuration"]
        k["lastOrder"] = "Empty"
        # print(f"Maintenance Data and current Load calculated for loomID {k['loomID']}.")
    
    # Calculate deliveryDate in minutes, add chainType

    for order in orders:
        # order["deliveryDate"] = order["deliveryDate"].replace(",", "")
        order["deliveryDate"] = order["deliveryDate"] + " 23:59:59"
        order["deliveryDate"] = datetime.strptime(order["deliveryDate"], '%m/%d/%Y %H:%M:%S')
        c = order["deliveryDate"] - dayStart
        order["deliveryDate"] = c.total_seconds() / 60
        order["chainType"] = str(order["chainID"]) + (order["type"])
        # print(f"Delivery date calculated for chainID {order['chainID']}.")
    pd.set_option("display.max_rows", None, "display.max_columns", None)
    orders_df = pd.DataFrame.from_dict(refined_data["orders"])
    looms_df = pd.DataFrame.from_dict(refined_data["looms"])

    # reconstruct ORDI orders
    order_columns = orders_df.columns
    orders_df = pd.concat([orders_df.query('status != "ORDI"'),
                           orders_df.query('status == "ORDI"').groupby('chainType', as_index=False)[order_columns]
                          .agg({'status': 'last', 'chainID': 'last', 'deliveryDate': 'last', 'type': 'last',
                                'priorityWeight': 'last', 'fabricType': 'last',
                                'ca': 'last', 'cc': 'last', 'strokesPerMt': 'last', 'yarns': 'last',
                                'drawing': 'last',
                                'variant': 'last', 'incom': 'last', 'comb': 'last', 'combHeight': 'last', 'ybf': 'last',
                                'targetMeters': 'sum'})]).reset_index(drop=True)

    # calculate metersLeft, loomEfficiency, currentLoad and lastOrder

    for i in range(len(orders_df)):
        if orders_df.loc[i, "incom"] <= 6:
            orders_df["loomEfficiency"] = 0.6
        elif orders_df.loc[i, "incom"] <= 14:
            orders_df["loomEfficiency"] = 0.5
        else:
            orders_df["loomEfficiency"] = 0.4
        # print(f"Loom Efficiency calculated for chainID {orders_df.loc[i,'chainID']}.")
        if orders_df.loc[i, "status"] == "ATEL":
            orders_df.loc[i, "metersLeft"] = orders_df.loc[i, "targetMeters"] - (
                    orders_df.loc[i, "kStrokes"] * 1000 / orders_df.loc[i, "strokesPerMt"])
            looms_df.loc[looms_df.loomID == orders_df.loc[i, "loomID"], "currentLoad"] += orders_df.loc[
                                                                                              i, "metersLeft"] * \
                                                                                          orders_df.loc[
                                                                                              i, "strokesPerMt"] * \
                                                                                          orders_df.loc[i, 'ybf'] / (
                                                                                                  orders_df.loc[
                                                                                                      i, "loomEfficiency"] *
                                                                                                  looms_df.loc[
                                                                                                      looms_df.loomID ==
                                                                                                      orders_df.loc[
                                                                                                          i, "loomID"], "loomSpeed"])

            looms_df.loc[looms_df.loomID == orders_df.loc[i, "loomID"], "lastOrder"] = orders_df.loc[i, "chainType"]
            looms_df.loc[looms_df.loomID == orders_df.loc[
                i, "loomID"], "withATEL"] = 1  # if atel on loom - will be used for loom sequence in the end
        else:
            orders_df.loc[i, "metersLeft"] = orders_df.loc[i, "targetMeters"]

    atel_orders = orders_df[orders_df.status == 'ATEL']  # keep ATEL orders
    orders_df = orders_df[orders_df.status != 'ATEL']
    looms_df['withATEL'].fillna(0, inplace=True)
    looms_df = looms_df.set_index('loomID')
    orders_df = orders_df.set_index('chainType')
    atel_orders = atel_orders.set_index('loomID')
    print(datetime.now(), " - End of Data Preprocessing.")
    '''
    print(datetime.now(), " - End of Objectives Calculator.")
    return solutions
