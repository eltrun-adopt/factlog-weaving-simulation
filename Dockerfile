#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

FROM python:3.7-slim
ENV RB_HOST $RB_HOST
ENV RB_PORT $RB_PORT
ENV RB_USER $RB_USER
ENV RB_PASS $RB_PASS

WORKDIR /piacenza-replication
COPY ./ ./
RUN pip install -r requirements.txt
ENTRYPOINT python replication_alg.py 1 $RB_HOST $RB_PORT $RB_USER $RB_PASS