#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
import datetime
import time
import pandas as pd
import repl_data_validation
import repl_calculator
import simulation_data_preprocessing
import simulation_past
import simulation_new



def repl_off():
    print(datetime.datetime.now(), " - Algorithm Start.")
    f = open('replication_input.json', )
    input = json.load(f)
    print(datetime.datetime.now(), " - Data Received Successfully.")

    uuid = 5 #input['uuid']
    # print(f"Process UUID: {input['uuid']}")
    # print(input['data'])
    input_data = input['data']
    # print(f["uuid"])

    # data validation
    problems = {}
    #try:
    problems['data'] = {}
    data_ok, problems['data']['message'] = repl_data_validation.repl_data_validation(input_data)

    if data_ok:

        repl_solutions = repl_calculator.repl_calculator(input_data)

        machines_df, orders_df, workers, startDate, endDate, dsl = simulation_data_preprocessing.simulation_data_preprocessing(
            input_data)

        solutions_past, new_orders_df = simulation_past.simulation_past(machines_df, orders_df, workers, dsl)

        solutions_new = simulation_new.simulation_new(machines_df, new_orders_df, workers, dsl)

        solutions_sim = {}
        for key,val in solutions_past.items():
            if key != 'total_orders':
                solutions_sim[key] = round((solutions_new[key] - solutions_past[key])/solutions_past[key] * 100,3)
            else:
                solutions_sim[key] = solutions_new[key]
        # print(solutions)
        # solutions_dict['uuid'] = uuid
        output = {}
        output['data'] = {}
        output['data']['replication'] = {}
        output['data']['simulation'] = {}
        for key, val in repl_solutions.items():
            if key != 'total_orders':
                output['data']['replication'][key] = val
            else:
                output['data']['replication'][key] = solutions_new['total_orders']
        for key, val in solutions_sim.items():
            output['data']['simulation'][key] = val
        output['produced_at'] = int(time.time() * 1000)
        output['uuid'] = uuid
        # repl_output = json.dumps(repl_solutions)
        filename = "replication_output2.json"
        with open(filename, 'w') as outfile:
            json.dump(output, outfile)
        print(f"{datetime.datetime.now()}  - Algorithm End.")

    else:
        print(f"{datetime.datetime.now()}  - Incorrect Input - data failed validation stage.")
        problems['uuid'] = uuid
        problems['produced_at'] = int(time.time() * 1000)
        # output = json.dump(problems)
        filename = "replication_output_problems.json"
        with open(filename, 'w') as outfile:
            json.dump(problems, outfile)
        print(f"{datetime.datetime.now()}  - Algorithm End.")

    '''
    except Exception as e:
        print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
        print(str(e))
        error_message = {"message": str(e)}
        error_response = {"uuid": uuid, "data": error_message, "produced_at": int(time.time() * 1000)}
        filename = "replication_output_exception.json"
        with open(filename, 'w') as outfile:
            json.dump(error_response, outfile)
        print(f"{datetime.datetime.now()}  - Algorithm End.")
    '''


repl_off()
