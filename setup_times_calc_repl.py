#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import pandas as pd
from datetime import datetime


def setup_times_calculator_repl(new_orders,orders_df, last, day):
    print(f"{datetime.now()}  - Start of setup times calculation: Day {day}.")
    order_col = list(new_orders.index)
    order_col = list(dict.fromkeys(order_col))
    order_row = order_col.copy()
    for i in last:
        order_row.append(i)
    order_row = list(dict.fromkeys(order_row))
    setup_times = pd.DataFrame(columns=order_col, index=order_row)
    for i in order_row:
        for j in order_col:
            # print (i,j)
            if i == j:
                setup_times.loc[i, j] = 0
            elif i == "00000":
                setup_times.loc[i, j] = 0
            else:
                setup_times.loc[i, j] = knotting(orders_df.loc[j, "ca"], orders_df.loc[j, "cc"],
                                                 orders_df.loc[j, "yarns"], orders_df.loc[j, "comb"],
                                                 orders_df.loc[j, "comb_height"], orders_df.loc[i, "ca"],
                                                 orders_df.loc[i, "cc"],
                                                 orders_df.loc[i, "yarns"], orders_df.loc[i, "comb"],
                                                 orders_df.loc[i, "comb_height"])

    print(f"{datetime.now()}  - End of setup times calculation: Day {day}.")
    return setup_times


def knotting(ca_new, cc_new, yarns_new, comb_new, combHeight_new, ca_old, cc_old, yarns_old, comb_old, combHeight_old):
    if (ca_new == ca_old) and (
            cc_new == cc_old):
        setup = 0
    elif (ca_new == ca_old) or (
            yarns_new == yarns_old and (
            comb_new == comb_old)):
        # KNOTTING SETUP TYPE
        if yarns_new <= 3000:
            # EASY KNOTTING
            setup = 4 * 60
        elif yarns_new <= 5000:
            # MEDIUM KNOTTING
            setup = 4 * 60
        else:
            # HARD KNOTTING
            setup = 6 * 60
    else:
        # FULL SETUP
        if abs(combHeight_new - combHeight_old) <= 10:
            # EASY CHANGE
            setup = 4 * 60
        elif abs(combHeight_new - combHeight_old) <= 15:
            # MEDIUM CHANGE
            setup = 6 * 60
        else:
            # HARD CHANGE
            setup = 8 * 60

    return setup
