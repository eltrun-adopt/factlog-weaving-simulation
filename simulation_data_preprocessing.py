#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd
from datetime import datetime

pd.set_option("display.max_rows", None, "display.max_columns", None)


def simulation_data_preprocessing(refined_data):
    print(datetime.now(), " - Start of Data Preprocessing.")

    # Start Date

    startDate = refined_data["startDate"]
    startDate = datetime.strptime(startDate, '%m/%d/%Y %H:%M:%S')

    # End Date

    endDate = refined_data["endDate"]
    endDate = datetime.strptime(endDate, '%m/%d/%Y %H:%M:%S')

    # workers

    workers = refined_data["working_groups"]

    # DSL

    dsl = refined_data['daily_setup_time_limit']

    hist_data = pd.DataFrame.from_dict(refined_data["historical_data"]).T

    # machines

    b = hist_data.loom_id.unique().tolist()
    machines_df = pd.DataFrame(index=b, columns=['loomSpeed'])
    for m in machines_df.index:
        dummy_df = hist_data.loc[hist_data['loom_id'] == m].reset_index(drop=True)
        machines_df.loc[m, 'loomSpeed'] = dummy_df.loc[0, 'loom_speed']

    # orders

    for i in hist_data.index:
        # order["deliveryDate"] = order["deliveryDate"].replace(",", "")
        hist_data.loc[i, "delivery_date"] = hist_data.loc[i, "delivery_date"] + " 23:59:59"
        hist_data.loc[i, "delivery_date"] = datetime.strptime(hist_data.loc[i, "delivery_date"], '%d/%m/%Y %H:%M:%S')
        c = hist_data.loc[i, "delivery_date"] - startDate
        hist_data.loc[i, "delivery_date"] = c.total_seconds() / 60
        hist_data.loc[i, "chainType"] = str(hist_data.loc[i, "chain_id"]) + (hist_data.loc[i, "type"])
        hist_data.loc[i, "chainTypeLoom"] = hist_data.loc[i, "chainType"] + str(hist_data.loc[i, "loom_id"])
        # print(f"Delivery date calculated for chainID {order['chainID']}.")

    orders_df = pd.DataFrame(index=hist_data.chainTypeLoom.unique(),
                             columns=['chain_id', 'chainType', 'loom_id', 'start_date', 'k_strokes', 'ca', 'cc',
                                      'strokes_per_mt', 'yarns', 'delivery_date', 'length_mt', 'type', 'incom', 'comb',
                                      'comb_height', 'ybf'])

    for i in orders_df.index:
        dummy_df = hist_data.loc[hist_data['chainTypeLoom'] == i]
        dummy_df['start_timestamp'] = pd.to_datetime(dummy_df['start_timestamp'], format='%d/%m/%Y %H:%M')
        orders_df.loc[i, 'chain_id'] = dummy_df.iloc[0, 0]
        orders_df.loc[i, 'chainType'] = dummy_df.iloc[0, 24]
        orders_df.loc[i, 'type'] = dummy_df.iloc[0, 15]
        orders_df.loc[i, 'loom_id'] = dummy_df.iloc[0, 1]
        orders_df.loc[i, 'start_date'] = dummy_df['start_timestamp'].min()
        orders_df.loc[i, 'start_date'] = orders_df.loc[i, 'start_date'].normalize()
        orders_df.loc[i, 'start_date'] = max(0, (orders_df.loc[i, 'start_date'] - startDate).total_seconds() / 60)
        orders_df.loc[i, 'strokes_per_mt'] = dummy_df['strokes_per_mt'].max()
        orders_df.loc[i, 'k_strokes'] = dummy_df['k_strokes'].sum()
        orders_df.loc[i, 'delivery_date'] = dummy_df['delivery_date'].min()
        orders_df.loc[i, 'length_mt'] = orders_df.loc[i, 'k_strokes'] / orders_df.loc[i, 'strokes_per_mt'] * 1000
        orders_df.loc[i, 'ca'] = dummy_df['ca'].min()
        orders_df.loc[i, 'cc'] = dummy_df['cc'].max()
        orders_df.loc[i, 'incom'] = dummy_df['incom'].min()
        orders_df.loc[i, 'comb'] = dummy_df['comb'].min()
        orders_df.loc[i, 'comb_height'] = dummy_df['comb_height'].min()
        orders_df.loc[i, 'ybf'] = dummy_df['ybf'].min()
        orders_df.loc[i, 'yarns'] = dummy_df['yarns'].min()

    # print(orders_df)

    '''

    # reconstruct ORDI orders
    order_columns = orders_df.columns
    orders_df = pd.concat([orders_df.query('status != "ORDI"'),
                           orders_df.query('status == "ORDI"').groupby('chainType', as_index=False)[order_columns]
                          .agg({'status': 'last', 'chainID': 'last', 'deliveryDate': 'last', 'type': 'last',
                                'priorityWeight': 'last', 'fabricType': 'last',
                                'ca': 'last', 'cc': 'last', 'strokesPerMt': 'last', 'yarns': 'last',
                                'drawing': 'last',
                                'variant': 'last', 'incom': 'last', 'comb': 'last', 'combHeight': 'last', 'ybf': 'last',
                                'targetMeters': 'sum'})]).reset_index(drop=True)

    # calculate metersLeft, loomEfficiency, currentLoad and lastOrder

    for i in range(len(orders_df)):
        if orders_df.loc[i, "incom"] <= 6:
            orders_df["loomEfficiency"] = 0.6
        elif orders_df.loc[i, "incom"] <= 14:
            orders_df["loomEfficiency"] = 0.5
        else:
            orders_df["loomEfficiency"] = 0.4
        # print(f"Loom Efficiency calculated for chainID {orders_df.loc[i,'chainID']}.")
        if orders_df.loc[i, "status"] == "ATEL":
            orders_df.loc[i, "metersLeft"] = orders_df.loc[i, "targetMeters"] - (
                    orders_df.loc[i, "kStrokes"] * 1000 / orders_df.loc[i, "strokesPerMt"])
            looms_df.loc[looms_df.loomID == orders_df.loc[i, "loomID"], "currentLoad"] += orders_df.loc[
                                                                                              i, "metersLeft"] * \
                                                                                          orders_df.loc[
                                                                                              i, "strokesPerMt"] * \
                                                                                          orders_df.loc[i, 'ybf'] / (
                                                                                                  orders_df.loc[
                                                                                                      i, "loomEfficiency"] *
                                                                                                  looms_df.loc[
                                                                                                      looms_df.loomID ==
                                                                                                      orders_df.loc[
                                                                                                          i, "loomID"], "loomSpeed"])

            looms_df.loc[looms_df.loomID == orders_df.loc[i, "loomID"], "lastOrder"] = orders_df.loc[i, "chainType"]
            looms_df.loc[looms_df.loomID == orders_df.loc[
                i, "loomID"], "withATEL"] = 1  # if atel on loom - will be used for loom sequence in the end
        else:
            orders_df.loc[i, "metersLeft"] = orders_df.loc[i, "targetMeters"]

    atel_orders = orders_df[orders_df.status == 'ATEL']  # keep ATEL orders
    orders_df = orders_df[orders_df.status != 'ATEL']
    looms_df['withATEL'].fillna(0, inplace=True)
    looms_df = looms_df.set_index('loomID')
    orders_df = orders_df.set_index('chainType')
    atel_orders = atel_orders.set_index('loomID')
    '''
    print(datetime.now(), " - End of Data Preprocessing.")

    return machines_df, orders_df, workers, startDate, endDate, dsl
