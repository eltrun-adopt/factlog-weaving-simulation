#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
import random

import pandas as pd
import numpy as np
import simplejson

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)


def csv_to_json():
    data = {}
    data['startDate'] = "11/25/2020 00:00:00"
    data['endDate'] = "01/29/2021 23:59:59"
    data['working_groups'] = 1
    data['daily_setup_time_limit'] = 1000
    data['historical_data'] = {}
    df = pd.read_csv('replication_data_sample.csv', sep=';', index_col='id')
    # df = df.drop(labels=['start_timestamp', 'end_timestamp'], axis=1)
    print (df.loom_id.unique())
    print (df.shape)
    speeds = {}
    for m in df.loom_id.unique():
        speeds[m] =  random.choice([280,300,400])
    for i in df.index:
        df.loc[i,'loom_speed'] = speeds[df.loc[i,'loom_id']]
    df['ybf'] = 1.1
    print(df.head())
    for i in df.index:
        data['historical_data'][i] = {}
        for j in df.columns:
            # print(type(df.loc[i, j]))
            if isinstance(df.loc[i, j], np.float64):
                data['historical_data'][i][j] = float(df.loc[i, j])
            elif isinstance(df.loc[i, j], np.int64):
                data['historical_data'][i][j] = int(df.loc[i, j])
            else:
                data['historical_data'][i][j] = df.loc[i, j]
            # print (i,j)
    # print(data)
    # df.to_json(r'test.json', orient='index')
    input = {}
    input['data'] = data
    #input['uuid'] = 10
    #input['processID'] = 100
    filename = f"replication_input.json"
    with open(filename, 'w') as outfile:
        simplejson.dump(input, outfile, ignore_nan=True)


def read_json():
    f = open('replication_input.json', )
    hist_dict = json.load(f)
    for key in hist_dict.keys():
        print(hist_dict[key])


def output_json():
    data = {}
    data['replication'] = {}
    data['simulation'] = {}
    data['replication']['total_orders'] = 30
    data['replication']['completed_orders'] = 20
    data['replication']['tardiness'] = 700.5
    data['replication']['tardy_jobs'] = 10
    data['replication']['energy_consumption'] = 20.5
    data['simulation']['makespan'] = -10.5
    data['simulation']['energy_consumption'] = -2.5
    data['simulation']['tardiness'] = -10.7
    data['simulation']['tardy_jobs'] = -20.7
    filename = f"replication_output.json"
    with open(filename, 'w') as outfile:
        simplejson.dump(data, outfile, ignore_nan=True)


csv_to_json()
#output_json()
