#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import math
import numpy as np
import pandas as pd
from datetime import datetime
import setup_times_calc_repl
import workers_availability_repl

pd.set_option("display.max_rows", None, "display.max_columns", None)


def simulation_new(machines_df, orders_df, workers, dsl):
    print(datetime.now(), " - Start of Optimization.")
    daily_setup_times = [0] * 1000
    workers_list = [[math.inf] for i in range(workers)]
    solutions_new = {}

    orders_df = orders_df.drop(
        columns=['loom_id', 'k_strokes', 'process_time', 'setup_time', 'end_time', 'total_end', 'type'])
    dummy_df = orders_df.drop('chainType', 1)
    dummy_df = dummy_df.astype(float)

    orders_df = pd.concat([orders_df['chainType'], dummy_df], axis=1)

    orders_df = orders_df.groupby('chainType').agg(
        {'length_mt': 'sum', 'strokes_per_mt': 'mean', 'ca': 'mean', 'cc': 'mean', 'start_date': 'mean',
         'delivery_date': 'mean', 'yarns': 'mean', 'comb': 'mean', 'comb_height': 'mean', 'incom': 'mean',
         'ybf': 'mean', 'loomeff': 'mean'})
    orders_df['chainType'] = orders_df.index
    process_times = pd.DataFrame(0, index=orders_df.index, columns=machines_df.index)
    for i in orders_df.index:
        orders_df.loc[i,'start_date'] -= 5760
        for m in machines_df.index:
            process_times.loc[i, m] = (orders_df.loc[i, 'length_mt'] * orders_df.loc[i, 'strokes_per_mt'] *
                                       orders_df.loc[i, 'ybf']) / (
                                              orders_df.loc[i, 'loomeff'] * machines_df.loc[m, 'loomSpeed'])
    # print(process_times.head())
    # print (process_times)
    machines_df['last_order'] = "00000"
    machines_df['current_load'] = 0
    current_time = 0
    solutions_new['total_orders'] = len(orders_df.index)
    solutions_new['energy_consumption(%)'] = 0
    solutions_new['tardy_jobs(%)'] = 0
    solutions_new['tardiness(%)'] = 0

    earliest_worker_start = np.full((workers, 1), np.inf)
    jobs_left = list(orders_df.index)
    while len(jobs_left) > 0:
        print(f"{datetime.now()}  - Optimization - Start of Day {int(current_time / 1440 + 1)}.")
        dummy_df = orders_df.loc[orders_df['start_date'] <= current_time]
        dummy_df = dummy_df[dummy_df.index.isin(jobs_left)]
        dummy_df = dummy_df.sort_values(by='delivery_date')
        earliest_machine = pd.DataFrame(0, index=machines_df.index,
                                        columns=['earliest_start', 'earliest_end', 'worker'])
        earliest_machine['worker'] = 0
        earliest_machine['earliest_start'] = machines_df.current_load

        setup_times = setup_times_calc_repl.setup_times_calculator_repl(dummy_df, orders_df,
                                                                        machines_df.last_order.unique(),
                                                                        int(current_time / 1440 + 1))
        # print(dummy_df)
        for i in dummy_df.index:
            for m in machines_df.index:
                setup_time = setup_times.loc[machines_df.loc[m, 'last_order'], i]
                process_time = process_times.loc[i, m]
                if setup_time > 0:
                    for worker_resource in range(workers):
                        # print (workers_list)
                        earliest_worker_start[
                            worker_resource] = workers_availability_repl.workers_availability_repl(
                            daily_setup_times[:],
                            workers_list[worker_resource][:],
                            machines_df.loc[m, 'current_load'],
                            setup_time, dsl)
                    # print (earliest_worker_start,m)
                    chosen_worker = np.argmin(earliest_worker_start)
                    earliest_machine.loc[m, 'worker'] = chosen_worker
                    earliest_machine.loc[m, 'earliest_start'] = earliest_worker_start[chosen_worker]
                    earliest_machine.loc[m, 'earliest_end'] = earliest_worker_start[
                                                                  chosen_worker] + setup_time + process_time

                # ----------------------------------------------
                else:
                    earliest_machine.loc[m, 'earliest_end'] = machines_df.loc[
                                                                  m, 'current_load'] + setup_time + process_time

            if setup_time > 0:
                # print (earliest_machine)
                new_load = earliest_machine.earliest_end.min()
                loom_chosen = earliest_machine.earliest_end.idxmin()
                setup_start = earliest_machine.loc[loom_chosen, 'earliest_start']
                machines_df.loc[loom_chosen, 'current_load'] = new_load
                chosen_worker = earliest_machine.loc[loom_chosen, 'worker']
                workers_list[int(chosen_worker)].extend([setup_start,
                                                         setup_start + setup_time])
                workers_list[int(chosen_worker)].sort()
                daily_setup_times[math.floor((setup_start) / 1440)] += setup_time

            else:
                new_load = earliest_machine.earliest_end.min()
                loom_chosen = earliest_machine.earliest_end.idxmin()
                machines_df.loc[loom_chosen, 'current_load'] = new_load
            # loom_load[m] = starting_loom + setup_time + process_time
            # orders_df.loc[last_order, 'end_time'] = loom_load[m]
            machines_df.loc[loom_chosen, 'last_order'] = i
            if new_load > orders_df.loc[i, 'delivery_date']:
                solutions_new['tardiness(%)'] += new_load - orders_df.loc[i, 'delivery_date']
                solutions_new['tardy_jobs(%)'] += 1
            solutions_new['energy_consumption(%)'] += process_times.loc[i, loom_chosen] * 7.5 / 60
            jobs_left.remove(i)
        # print (machines_df.current_load)
        solutions_new['makespan(%)'] = machines_df.current_load.max()
        print(f"{datetime.now()}  - Optimization - End of Day {int(current_time / 1440 + 1)}.")
        current_time += 1440
        for m in machines_df.index:
            machines_df.loc[m, 'current_load'] = max(current_time, machines_df.loc[m, 'current_load'])

    print(datetime.now(), " - End of Optimization.")
    return solutions_new
