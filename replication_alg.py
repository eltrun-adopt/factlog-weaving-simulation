#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import json
import datetime
import time
import pandas as pd
import repl_data_validation
import repl_calculator
import simulation_data_preprocessing
import simulation_past
import simulation_new
import pika
import sys

online = sys.argv[1]
host = sys.argv[2]
port = sys.argv[3]
username = sys.argv[4]
password = sys.argv[5]

credentials = pika.PlainCredentials(username, password)
params = pika.ConnectionParameters(host, port, '/', credentials, heartbeat=1860, blocked_connection_timeout=930)
connection = pika.BlockingConnection(params)
channel = connection.channel()


def callback(ch, method, properties, body):
    print(datetime.datetime.now(), " - Algorithm Start.")
    # print(" [x] Received %r" % body.decode())

    input = json.loads(body)
    print(datetime.datetime.now(), " - Data Received Successfully.")
    uuid = input['uuid']
    print(f"Process UUID: {input['uuid']}")
    print(input['data'])
    input_data = input['data']
    # print(f["uuid"])

    try:
        # data validation
        problems = {}
        problems['data'] = {}
        data_ok, problems['data']['message'] = repl_data_validation.repl_data_validation(input_data)

        if data_ok:

            repl_solutions = repl_calculator.repl_calculator(input_data)

            machines_df, orders_df, workers, startDate, endDate, dsl = simulation_data_preprocessing.simulation_data_preprocessing(
                input_data)

            solutions_past, new_orders_df = simulation_past.simulation_past(machines_df, orders_df, workers, dsl)

            solutions_new = simulation_new.simulation_new(machines_df, new_orders_df, workers, dsl)

            solutions_sim = {}
            for key, val in solutions_past.items():
                if key != 'total_orders':
                    solutions_sim[key] = round((solutions_new[key] - solutions_past[key]) / solutions_past[key] * 100,
                                               3)
                else:
                    solutions_sim[key] = solutions_new[key]
            # print(solutions)
            # solutions_dict['uuid'] = uuid
            final_solutions = {}
            final_solutions['data'] = {}
            final_solutions['data']['replication'] = {}
            final_solutions['data']['simulation'] = {}
            for key, val in repl_solutions.items():
                if key != 'total_orders':
                    final_solutions['data']['replication'][key] = val
                else:
                    final_solutions['data']['replication'][key] = solutions_new['total_orders']
            for key, val in solutions_sim.items():
                final_solutions['data']['simulation'][key] = val
            final_solutions['produced_at'] = int(time.time() * 1000)
            final_solutions['uuid'] = uuid
            # print(solutions_dict['makespan'])
            output = json.dumps(final_solutions)
            print(f"{datetime.datetime.now()}  - Algorithm End.")
            channel.basic_publish(exchange='opt-result', routing_key='weaving-replication', body=output)

        else:
            print(f"{datetime.datetime.now()}  - Incorrect Input - data failed validation stage.")
            problems['uuid'] = uuid
            problems['produced_at'] = int(time.time() * 1000)
            output = json.dumps(problems)
            channel.basic_publish(exchange='opt-result', routing_key='weaving-replication', body=output)
            print(f"{datetime.datetime.now()}  - Algorithm End.")

    except Exception as e:
        print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
        print(str(e))
        error_message = {"message": str(e)}
        error_response = {"uuid": uuid, "data": error_message, "produced_at": int(time.time() * 1000)}
        error_response = json.dumps(error_response)
        channel.basic_publish(exchange='opt-result', routing_key='weaving-replication', body=error_response)
        print(f"{datetime.datetime.now()}  - Algorithm End.")


channel.basic_consume(queue='weaving-replication_job', on_message_callback=callback, auto_ack=True)
channel.start_consuming()
