#    factlog-weaving-simulation (c) by the Athens University of Economics and Business, Greece.
#
#    factlog-weaving-simulation is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

from datetime import datetime

import pandas as pd


def repl_data_validation(data_to_check):
    print(datetime.now(), " - Start of Data Validation.")

    checked_data = {}
    problems = {}
    problems['startDate'] = ''
    problems['endDate'] = ''
    problems['duration'] = ''
    problems['histData'] = []
    data_ok = False

    if isinstance(data_to_check, dict):
        # problems['fileFormat'] = "Correct file format."

        # check startDate - if it does not exist or is problematic, validation stops
        if 'startDate' in data_to_check.keys():
            start_date = data_to_check['startDate']
            try:
                start_date = datetime.strptime(start_date, '%m/%d/%Y %H:%M:%S')
            except Exception as e:
                problems[
                    "startDate"] = "Start Date has not valid format - should be of type string (mm/dd/YY HH:MM:SS)."
        else:
            problems["startDate"] = "Start Date must be included - should be of type string (mm/dd/YY HH:MM:SS)."

        if 'endDate' in data_to_check.keys():
            end_date = data_to_check['endDate']
            try:
                end_date = datetime.strptime(end_date, '%m/%d/%Y %H:%M:%S')

            except Exception as e:
                problems[
                    "startDate"] = "End Date has not valid format - should be of type string (mm/dd/YY HH:MM:SS)."
        else:
            problems["startDate"] = "End Date must be included - should be of type string (mm/dd/YY HH:MM:SS)."

        if len(problems['startDate']) == 0 and len(problems['endDate']) == 0:
            if (start_date - end_date).days >= 0:
                problems['duration'] = "Invalid time interval - end date should be after start date."
            else:
                input_data = data_to_check['historical_data']
                columns_str = ['type', 'start_timestamp', 'end_timestamp', 'delivery_date']
                columns_int = ['chain_id', 'loom_id']
                columns_float = ['k_strokes', 'working_hours', 'stop_hours', 'consumption_kwh',
                                 'strokes_per_mt', 'yarns', 'length_mt', 'incom', 'comb', 'comb_height',
                                 'loom_speed', 'ybf']
                for i in input_data.keys():
                    for j in (columns_int + columns_float + columns_str):
                        if j in columns_int:
                            problem = valid_field_int(j, input_data[i], i)
                            if len(problem) > 0:
                                problems['histData'].append(problem)
                        elif j in columns_float:
                            problem = valid_field_float(j, input_data[i], i)
                            if len(problem) > 0:
                                problems['histData'].append(problem)
                        elif j in columns_str:
                            problem = valid_field_str(j, input_data[i], i)
                            if len(problem) > 0:
                                problems['histData'].append(problem)
            if len(problems['duration']) == 0 and len(problems['endDate']) == 0 and len(
                    problems['startDate']) == 0 and len(problems['histData']) == 0:
                data_ok = True

    else:
        problems["fileFormat"] = "Invalid json format."
    print(datetime.now(), " - End of Data Validation.")
    return data_ok, problems


def check_dates(date1, date2):
    correct_format = False
    if type(date1) == str and type(date2) == str:
        date1 = date1.replace(",", "")
        date2 = date2.replace(",", "")
        try:
            date1 = datetime.strptime(date1, '%m/%d/%Y %H:%M:%S')
            date2 = datetime.strptime(date2, '%m/%d/%Y %H:%M:%S')
            if date1 > date2:
                correct_format = True
        except ValueError:
            correct_format = False
    elif date1 == None or date2 == None:
        correct_format = True
    return correct_format


def valid_field_int(name, row, i):
    problem = {}
    if name in row.keys():
        value = row[name]
        if isinstance(value, float) or isinstance(value, int):
            if int(value) != value or value < 0:
                problem['rowID'] = i
                problem['message'] = f"{name} value is {value} - not positive int."
        else:
            problem['rowID'] = i
            problem['message'] = f"{name} value is {type(value)} - not positive int."
    else:
        problem['rowID'] = i
        problem['message'] = f"{name} is not in keys - correct key {name}."
    return problem


def valid_field_float(name, row, i):
    problem = {}
    if name in row.keys():
        value = row[name]
        if (not isinstance(value, float) and not isinstance(value, int)) or value < 0 or (
                name == "strokes_per_mt" and value <= 0):
            problem['rowID'] = i
            problem['message'] = f"{name} value is {value} - not positive number."
    else:
        problem['rowID'] = i
        problem['message'] = f"{name} is not in keys - correct key {name}."
    return problem


def valid_field_str(name, row, i):
    problem = {}
    if name in row.keys():
        value = row[name]
        if name == 'delivery_date':
            if not isinstance(value, str):
                problem['rowID'] = i
                problem['message'] = f"{name} value is {value} - not string."
    else:
        problem['rowID'] = i
        problem['message'] = f"{name} is not in keys - correct key {name}."
    return problem
